<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 2/20/18
 * Time: 9:50 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Place
 * @package AppBundle\Entity
 * @ORM\Entity()
 */
class Place
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var
     * @ORM\Column(name="name" , type="string", nullable=true)
     */
    private $name;
    /**
     * @var
     * @ORM\Column(name="distance" , type="integer" ,nullable=true)
     */
    private $distance;

    /**
     * @var
     * @ORM\Column(name="trafic" , type="integer" ,nullable=true)
     */
    private $trafic;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param mixed $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * @return mixed
     */
    public function getTrafic()
    {
        return $this->trafic;
    }

    /**
     * @param mixed $trafic
     */
    public function setTrafic($trafic)
    {
        $this->trafic = $trafic;
    }


}