<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Place;
use AppBundle\Form\PlaceType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;

class PlacesController extends Controller
{
    /**
     * @Route("/Home", name="home")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $places = $em->getRepository("AppBundle:Place")->findAll();

        /*
        $session = new Session();
        //$session->start();

        //$session->set('name', json_encode($places));
        $r->getSession()->set('name', json_encode($places));



        $cookiesPlaces = array();

        foreach ($places as $key=>$value)
        {
            array_push($cookiesPlaces, $value->getName());
        }

        var_dump($cookiesPlaces);

        echo "xxxxxxxxxxxxxxx";

        echo serialize($cookiesPlaces);
        echo json_encode($cookiesPlaces);

        $res = new Response();

        $cookie = new Cookie('foo', serialize($cookiesPlaces), strtotime('now + 10 minutes'));
        $res->headers->setCookie($cookie);

        $res->send();

        $cookies=$request->cookies;

        $cookie= $cookies->get('foo');

        var_dump($cookie);
        */

        $form = $this->createForm(PlaceType::class);

        return $this->render("@App/index.html.twig",array(
            'palces'=>$places,
            'form' => $form->createView())
    );
    }
    /**
     * @Route("/new", name="new", options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function  newAction(Request $request){



        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        $form = $this->createForm(PlaceType::class, $user = new Place());
        $form->handleRequest($request);
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return new JsonResponse(array('message' => 'Success!'), 200);
        }
        $response = new JsonResponse(
            array(
                'message' => 'Error',
                'form' => $this->renderView('@App/new.html.twig',
                    array(
                        'form' => $form->createView(),
                    ))), 400);
        return $response;
    }
    /**
     * @Route("/edit", name="edit", options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function  editAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:Place')->find($id);
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        $form = $this->createForm(PlaceType::class, $user);
        $form->handleRequest($request);
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return new JsonResponse(array('message' => 'Success!'), 200);
        }
        $response = new JsonResponse(
            array(
                'message' => 'Error',
                'form' => $this->renderView('@App/edit.html.twig',
                    array(
                        'form' => $form->createView(),
                    ))), 400);
        return $response;
    }
    /**
     * @Route("/graph", name="graph")
     */
    public function GraphAction()
    {


        return $this->render("@App/Graph.html.twig"
        );
    }

    /**
     * @Route("/delete/{id}", name="delete", options={"expose"=true})
     * @param Place $id
     * @return Response
     */
    public function deleteUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $place = $em->getRepository("AppBundle:Place")->find($id);
        $em->remove($place);
        $em->flush();
        return new Response();
    }
    /**
     * @Route("/getAll")
     */
    public function ajaxAction(Request $request) {
        $students = $this->getDoctrine()
            ->getRepository('AppBundle:Place')
            ->findAll();

        if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {
            $jsonData = array();
            $idx = 0;
            foreach($students as $student) {
                $temp = array(
                    'name' => $student->getName(),
                    'address' => $student->getDistance()
                );
                $jsonData[$idx++] = $temp;
            }
            return new JsonResponse($jsonData);
        } else {
            return $this->render('student/ajax.html.twig');
        }
    }

}

