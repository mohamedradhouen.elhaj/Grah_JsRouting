$(document).ready(function () {

    $('#new').submit(function (e) {
        var newUrl = Routing.generate('new');
        e.preventDefault();

        $.ajax({
            type: $(this).attr('method'),
            url: newUrl,
            data: $(this).serialize(),
            success: function (response) {
                location.reload();
                console.log("editado");
            },
            error: function (response) {
                console.log("no editado");
            }
        });
    });
    $('.edit').submit(function (e) {
        var newUrl = Routing.generate('edit');
        e.preventDefault();

        $.ajax({
            type: $(this).attr('method'),
            url: newUrl,
            data: $(this).serialize(),
            success: function (response) {
                location.reload();
                console.log("editado");
            },
            error: function (response) {
                console.log("no editado");
            }
        });
    });
    $('.delete').click(function (e) {

        var getUrl = Routing.generate('delete', {'id': $(this).attr('id')});
        e.preventDefault();

        $.ajax({
            type: 'delete',
            url:  getUrl,
            data: $(this).serialize(),
            success: function (response) {
                location.reload();
                console.log("borrado");
            },
            error: function (response) {
                console.log("no borrado");
            }
        });
    });
});
